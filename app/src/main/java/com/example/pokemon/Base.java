package com.example.pokemon;

public class Base {
    private String base;

    public Base() {}

    public Base(String base) {
        this.base = base;

    }
    public String getBase() {
        return base;
    }

    public void setBase(String english) {
        this.base = english;
    }

    @Override
    public String toString() {
        return base;
    }
}

