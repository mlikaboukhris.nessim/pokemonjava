package com.example.pokemon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    Gson gson = new Gson();
    ArrayList<Pokemon> pokemons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<Pokemon>Pokemons =new ArrayList<Pokemon>();
        ListView pokemonList = (ListView) findViewById(R.id.pokemon_list);
        pokemonList.setAdapter(new PokemonAdapter(getApplicationContext(), pokemons));
    }


    private ArrayList<Pokemon> buildPokemonsList() {
        Type listType = new TypeToken<ArrayList<Pokemon>>(){}.getType();
        return gson.fromJson(readJSONFile("pokedex.json"), listType);
    }

    private String readJSONFile(String filename) {
        AssetManager am = this.getAssets();
        String result = "";

        try {
            InputStream inputStream = am.open(filename);
            byte[] b = new byte[inputStream.available()];
            inputStream.read(b);
            result = new String(b);
        } catch (Exception e) {
            Log.e("Error readJSONFile", e.getMessage());
        }
        // Log.i("Result",result) ;
        return result;
    }



}